import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import { Col, Row } from 'react-bootstrap';

export default function User({productsData}) {

    const [ products, setProducts ] = useState([])

    useEffect(() =>{
        const productsArr = productsData.map(product => {
            if(product.isctive === true) {
                return(
                    <Col xs={12} md={3} className=''>
                        <ProductCard key ={product._id} productProp={product} />
                    </Col>
                )
            }else {
                return null;
            }
        })

        setProducts(productsArr)
    }, [productsData])

    return(
        <Row>
            { products }
        </Row>
    )
}
